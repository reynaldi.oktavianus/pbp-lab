from django.urls import path

from lab_2.views import Note
from .views import index, add_note, note_list

urlpatterns = [
    path('', index, name='index'),
    path('Notes', Note),
    path('add-note', add_note),
    path('note-list', note_list)
    
]