asgiref==3.4.1
astroid==2.5.1
colorama==0.4.4
dj-database-url==0.5.0
Django==3.2.7
isort==5.7.0
lazy-object-proxy==1.5.2
mccabe==0.6.1
pylint==2.7.2
pytz==2021.1
sqlparse==0.4.2
toml==0.10.2
wrapt==1.12.1
coverage

