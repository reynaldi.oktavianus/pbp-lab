1. The *difference* between JSON and XML is:
    XML always need an opener and close like `<user></user>`. However the JSON is more simple so the JSON could be more understand than XML. Also sometimes the JSON have more light file than the XML

2. The difference between HMTL and XML is:
    The HTML is have a tag that we need to be use. BUt mainly the difference is the XML is more likely showing us the data structure and based on the context

```json
[{"model": "lab_2.note", "pk": 1, "fields": {"to": "Anastasia Audi Wulandari Zebua", "from_siapa": "Reynaldi Oktavianus", "title": "Kapan Nikah", "message": "Brok kapan nikah brok"}}, {"model": "lab_2.note", "pk": 2, "fields": {"to": "aldi", "from_siapa": "audi", "title": "makan siang", "message": "beliin gw"}}]
```
```xml
<django-objects version="1.0">
<object model="lab_2.note" pk="1">
<field name="to" type="CharField">Anastasia Audi Wulandari Zebua</field>
<field name="from_siapa" type="CharField">Reynaldi Oktavianus</field>
<field name="title" type="CharField">Kapan Nikah</field>
<field name="message" type="CharField">Brok kapan nikah brok</field>
</object>
<object model="lab_2.note" pk="2">
<field name="to" type="CharField">aldi</field>
<field name="from_siapa" type="CharField">audi</field>
<field name="title" type="CharField">makan siang</field>
<field name="message" type="CharField">beliin gw</field>
</object>
</django-objects>
```