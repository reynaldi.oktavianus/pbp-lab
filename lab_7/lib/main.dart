import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Mutuals',
        theme: ThemeData(
          primarySwatch: Colors.yellow,
        ),
        home: FirstPage());
  }
}

class FirstPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: const Text('Mutuals'),
      ),
      body: Center(
        // Center is a layout widget. It takes a single child and positions it
        // in the middle of the parent.
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            const Text('Welcome To Mutuals',
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontSize: 30, height: 5, fontWeight: FontWeight.bold)),
            const Text(
              'Mutuals is a web application that focuses in connecting end user to a diverse group of people sharing the same interests. You may message them through the message box feature, or have fun with our shuffle button and communicate with randomly selected users based on your shared interests.',
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 20,
                height: 1.5,
              ),
            ),
            ElevatedButton(
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => SecondPage()),
                );
              },
              child: Text(
                "Let's be Mutuals",
                style: TextStyle(
                  fontSize: 22,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class SecondPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("Sign In"),
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                "Sign in and let's be friends",
                style: TextStyle(
                    fontSize: 22, height: 3, fontWeight: FontWeight.bold),
              ),
              const Padding(
                padding: EdgeInsets.symmetric(horizontal: 8, vertical: 16),
                child: TextField(
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    hintText: 'username',
                  ),
                ),
              ),
              Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 8, vertical: 16),
                  child: TextField(
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      hintText: 'password',
                    ),
                  )),
              ElevatedButton(
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => FourPage()),
                    );
                  },
                  child: Text(
                    "Submit",
                    style: TextStyle(fontSize: 22),
                  )),
              Text(
                "Haven't any account?",
                style: TextStyle(fontSize: 20, height: 2),
              ),
              ElevatedButton(
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => ThirdPage()),
                    );
                  },
                  child: Text(
                    "Sign Up!",
                    style: TextStyle(fontSize: 22),
                  ))
            ],
          ),
        ));
  }
}

class ThirdPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: const Text('Sign Up')),
        body: Center(
          child: Column(
            children: <Widget>[
              Text("Let's sign up to continue to Mutuals",
                  style: TextStyle(
                      fontSize: 22, height: 3, fontWeight: FontWeight.bold)),
              const Padding(
                padding: EdgeInsets.symmetric(horizontal: 10, vertical: 12),
                child: TextField(
                  decoration: InputDecoration(
                      border: OutlineInputBorder(), hintText: 'Fullname'),
                ),
              ),
              Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 10, vertical: 12),
                child: TextField(
                  decoration: const InputDecoration(
                      border: OutlineInputBorder(), hintText: 'Username'),
                ),
              ),
              Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 10, vertical: 12),
                child: TextField(
                  decoration: const InputDecoration(
                      border: OutlineInputBorder(), hintText: 'Password'),
                ),
              ),
              Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 10, vertical: 12),
                child: TextField(
                  decoration: const InputDecoration(
                      border: OutlineInputBorder(),
                      hintText: 'Retype Password'),
                ),
              ),
              ElevatedButton(
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => FifthPage()),
                    );
                  },
                  child: Text(
                    'Continue',
                    style: TextStyle(fontSize: 22),
                  ))
            ],
          ),
        ));
  }
}

class FourPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: const Text("Profile")),
        body: SingleChildScrollView(
            child: Column(
          children: <Widget>[
            Text(
              "Hi, Name",
              style: TextStyle(
                  fontSize: 30, height: 3, fontWeight: FontWeight.bold),
            ),
            const Padding(
              padding: EdgeInsets.symmetric(horizontal: 10, vertical: 12),
              child: TextField(
                decoration: InputDecoration(
                    border: OutlineInputBorder(), hintText: 'Fullname'),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 12),
              child: TextField(
                decoration: const InputDecoration(
                    border: OutlineInputBorder(), hintText: 'YYYY-MM-DD'),
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 10, vertical: 12),
              child: TextField(
                decoration: const InputDecoration(
                    border: OutlineInputBorder(), hintText: 'Domicile'),
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 10, vertical: 12),
              child: TextField(
                decoration: const InputDecoration(
                  border: OutlineInputBorder(),
                  hintText: 'LineID',
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 10, vertical: 12),
              child: TextField(
                decoration: const InputDecoration(
                  border: OutlineInputBorder(),
                  hintText: 'Instagram',
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 10, vertical: 12),
              child: TextField(
                decoration: const InputDecoration(
                  border: OutlineInputBorder(),
                  hintText: 'Phone Number',
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 10, vertical: 12),
              child: TextField(
                decoration: const InputDecoration(
                  border: OutlineInputBorder(),
                  hintText: 'email',
                ),
              ),
            ),
            ElevatedButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => SixthPage()),
                  );
                },
                child: Text(
                  'Submit',
                  style: TextStyle(fontSize: 22),
                ))
          ],
        )));
  }
}

class FifthPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: const Text("Profile")),
        body: SingleChildScrollView(
            child: Column(
          children: <Widget>[
            Text('Continue to start using Mutuals!',
                style: TextStyle(
                    fontSize: 22, height: 3, fontWeight: FontWeight.bold)),
            const Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 12),
              child: TextField(
                decoration: const InputDecoration(
                    border: OutlineInputBorder(), hintText: 'YYYY-MM-DD'),
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 10, vertical: 12),
              child: TextField(
                decoration: const InputDecoration(
                    border: OutlineInputBorder(), hintText: 'Domicile'),
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 10, vertical: 12),
              child: TextField(
                decoration: const InputDecoration(
                  border: OutlineInputBorder(),
                  hintText: 'LineID',
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 10, vertical: 12),
              child: TextField(
                decoration: const InputDecoration(
                  border: OutlineInputBorder(),
                  hintText: 'Instagram',
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 10, vertical: 12),
              child: TextField(
                decoration: const InputDecoration(
                  border: OutlineInputBorder(),
                  hintText: 'Phone Number',
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 10, vertical: 12),
              child: TextField(
                decoration: const InputDecoration(
                  border: OutlineInputBorder(),
                  hintText: 'email',
                ),
              ),
            ),
            ElevatedButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => SixthPage()),
                  );
                },
                child: Text(
                  'Submit',
                  style: TextStyle(fontSize: 22),
                ))
          ],
        )));
  }
}

class SixthPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text("Homepage")),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Text("Hi, Name!",
                style: TextStyle(
                    fontSize: 30, height: 3, fontWeight: FontWeight.bold)),
            Text(
              "Welcome to your dashboard!",
              style: TextStyle(
                  fontSize: 25, height: 2, fontWeight: FontWeight.bold),
            ),
            Text(
              'Please choose what friends do you want connected with!',
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 22, height: 1),
            ),
            ElevatedButton(
                style: ElevatedButton.styleFrom(
                  minimumSize: const Size(200, 50),
                  maximumSize: const Size(400, 50),
                ),
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => User1()),
                  );
                },
                child: Text(
                  'Ryan Putra\nPlaying Games, Singing',
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 22),
                )),
            ElevatedButton(
              style: ElevatedButton.styleFrom(
                minimumSize: const Size(200, 50),
                maximumSize: const Size(400, 50),
              ),
              onPressed: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => User2()));
              },
              child: Text(
                'Anastasia Audi\nUI/UX, Design, Food Vlogger',
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 22),
              ),
            ),
            ElevatedButton(
              style: ElevatedButton.styleFrom(
                minimumSize: const Size(200, 50),
                maximumSize: const Size(500, 50),
              ),
              onPressed: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => User3()));
              },
              child: Text(
                'Kevin Haryanto\nMusic, Playing Football',
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 22),
              ),
            ),
            ElevatedButton(
              style: ElevatedButton.styleFrom(
                minimumSize: const Size(200, 50),
                maximumSize: const Size(400, 50),
              ),
              onPressed: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => User4()));
              },
              child: Text(
                'Joel Andika\nTravelling, Playing Guitar',
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 22),
              ),
            ),
            ElevatedButton(
              style: ElevatedButton.styleFrom(
                minimumSize: const Size(200, 50),
                maximumSize: const Size(400, 50),
              ),
              onPressed: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => User5()));
              },
              child: Text(
                'Leonard Felix\nStudy, Playing Basketball',
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 22),
              ),
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => FirstPage()),
          );
        },
        child: const Text(
          "Log Out",
          textAlign: TextAlign.center,
          style: TextStyle(fontSize: 18),
        ),
        backgroundColor: Colors.yellow,
      ),
    );
  }
}

class User1 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: const Text("Ryan Putra")),
        body: Center(
          child: Container(
              width: 350,
              height: 180,
              color: Colors.yellow[50],
                child: const Text('Fullname : Ryan Putra Budianto\nHobby :Playing Games,Singing\nDomicile : Jakarta Barat\nLine ID : ryan_budianto\nInstagram : v1ct1n', style: TextStyle(fontSize: 22),)
                ),
      )
    );
  }
}

class User2 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: const Text("Anastasia Audi")),
        body: Center(
          child: Container(
              width: 350,
              height: 180,
              color: Colors.yellow[50],
                child: const Text('Fullname : Anastasia Audi Wulandari\nHobby :UI/UX, Designer, Food Vlogger\nDomicile : Serpong\nLine ID : anastasiaaaudiw\nInstagram : aut1s', style: TextStyle(fontSize: 22),)
                ),
      )
    );
  }
}

class User3 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: const Text("Kevin Haryanto")),
        body: Center(
          child: Container(
              width: 350,
              height: 180,
              color: Colors.yellow[50],
                child: const Text('Fullname : Kevin Haryanto\nHobby :Music, Playing Football\nDomicile : Jakarta Utara\nLine ID : kevin_h24\nInstagram : kevin_h24', style: TextStyle(fontSize: 22),)
                ),
      )
    );
  }
}

class User4 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: const Text("Joel Andika")),
        body: Center(
          child: Container(
              width: 350,
              height: 180,
              color: Colors.yellow[50],
                child: const Text('Fullname : Joel Andika\nHobby :Travelling, Playing Guitar\nDomicile : Tangerang\nLine ID : joel_adhk\nInstagram : joel_adhk', style: TextStyle(fontSize: 22),)
                ),
      )
    );
  }
}

class User5 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: const Text("Leonard Felix")),
        body: Center(
          child: Container(
              width: 350,
              height: 180,
              color: Colors.yellow[50],
                child: const Text('Fullname : Leonard Felix Sadewa\nHobby :Study, Playing Basketball\nDomicile : Serpong Utara\nLine ID : leo_felix\nInstagram : leo_felix', style: TextStyle(fontSize: 22),)
                ),
      )
    );
  }
}