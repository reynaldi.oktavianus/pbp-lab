from django.urls import path

from lab_1.views import friend_list
from .views import index, add_friend

urlpatterns = [
    path('', index, name='index'),
    path('friends', friend_list),
    path('add', add_friend),
    
]
app_name = 'lab_3'