from django.shortcuts import render, redirect
from .forms import FriendForm
from lab_1.models import Friend
from django.contrib.auth.decorators import login_required

@login_required(login_url='/admin/login/')
def index(request):
    friends = Friend.objects.all()  # TODO Implement this
    response = {'friends': friends}
    return render(request, 'lab3_index.html', response)

@login_required(login_url='/admin/login/')
def add_friend(request):
    response = {}
    form = FriendForm(request.POST or None)
    print(request.POST) 

    # check if request data is POST
    if request.method == "POST" and form.is_valid():
        # save the form data to model
        form.save()
        return redirect('lab_3:index') # redirect to table that shows friends

    response['form'] = form

    return render(request, 'lab3_form.html', response)
 
    

