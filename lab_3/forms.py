from django.forms import ModelForm
from django.db import models
from lab_1.models import Friend

class FriendForm(ModelForm):
    class Meta:
            model = Friend
            fields = "__all__"   